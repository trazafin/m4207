 
 #define LED 2  // On indique où est branchée la LED
 void setup()                    // run once, when the sketch starts
{
  pinMode(LED, OUTPUT);
}

void loop()                       // run over and over again
{
 digitalWrite(LED, HIGH);   // turn on the LED 
 delay(500);                       
 digitalWrite(LED, LOW);    // turn off the LED
 delay(500);                       
}
