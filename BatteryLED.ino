#include <LBattery.h>
 #define LED 2  // On indique où est branchée la LED
char buff[256];

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(LED, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  sprintf(buff,"battery level = %d", LBattery.level() );
  Serial.println(buff);
  sprintf(buff,"is charging = %d",LBattery.isCharging() );
  Serial.println(buff);
 delay(1000); 

if (LBattery.level() <= 15) {
 digitalWrite(LED, HIGH);   // turn on the LED 
 delay(500);                       
 digitalWrite(LED, LOW);    // turn off the LED
 delay(500);                       
}
}

